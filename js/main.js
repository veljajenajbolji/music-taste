

let forma = document.getElementById("forma");
//https://tastedive.com/api/similar?q=code+orange&verbose=1&type=music

forma.addEventListener("submit", function getQuery(){
    event.preventDefault();
    var searchQuery = searchBar.value;

    let queryArray = searchQuery.split(" ")
    let filteredQueryArray = queryArray.filter(function(e){
        return e != "";
    })
    let q = filteredQueryArray.join("+");
    let searchableQuery = `https://tastedive.com/api/similar?q=${q}&verbose=1&type=music&k=421974-musictas-2JCVA89T`
    getReccomendations(searchableQuery);


})

function getReccomendations(query){

    $.ajax({
        url: query,
        type: "get",
        dataType: "jsonp",
        success: function(result) {
            console.log(result);
            console.log(result.Similar.Results);
            try{
                $('#reccomendation-container').slick("unslick");   
                          

            }catch(e){
                console.log("first visit")
            }
            displayResults(result.Similar.Results)
            expandArtist(result.Similar.Results)

        },
        error: function(xhr,status, error) {  
            console.log(error); 
        }
    });
}
let container = document.getElementById("reccomendation-container")
function displayResults(results){
    if(results.length < 1){
        container.innerHTML = "";
    }else{
        document.getElementById("reccomendation-container").style.display="grid"
        container.innerHTML = "";

    }

    results.forEach(result => {

        if(result.yUrl){
        container.innerHTML+=`
        <div class="reccomendation ripple" data-yID = ${result.yID} > 
            <div class="info-overlay ">
                <div class="info">
                    <h5 class d-inline>${result.Name}</h5>
                    <p>${result.wTeaser.substr(0,200) + "..."}</p>
                </div>
            </div>
        
        </div>
                `
        }

        console.log(result.yID)
        document.querySelector(".reccomendation:last-child").style.backgroundImage = "url('https://img.youtube.com/vi/"+result.yID+"/hqdefault.jpg')"
    });
    // $('#reccomendation-container').slick("unslick");
			$("#explanation").removeClass("d-none");
    activateSlick();



    // container.innerHTML = writeOut;


}
    function activateSlick(){
        //slick slider activate
        $('#reccomendation-container').slick({
            slidesToShow: 3,
            infnite: true,
            slidesToScroll: 2,
            cssEase: 'linear',
            responsive: [
            {
                breakpoint: 1400,
                settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1

            
            }
            },
            {
                breakpoint: 480,
                settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
                }
            }
            ]
        });
    }
